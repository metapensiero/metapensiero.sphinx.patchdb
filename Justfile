# -*- coding: utf-8 -*-
# :Project:   PatchDB — Development tasks
# :Created:   sab 29 apr 2023, 19:13:40
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   GNU General Public License version 3 or later
# :Copyright: © 2023, 2024 Lele Gaifax
#

set dotenv-load := false

package-name := `python -c "from tomllib import load;print(load(open('pyproject.toml', 'rb'))['project']['name'])"`
package-version := `bump-my-version show current_version`
dist-file-name := replace(package-name, ".", "_") + "-" + package-version

# List available targets
@_help:
  just --list


#########
# TESTS #
#########

# Run tests
check *flags:
  #!/usr/bin/env bash

  pytest {{flags}}
  status=$?
  tests/postgresql stop
  exit $status


########
# I18N #
########

bugs-email-address := 'lele@metapensiero.it'
copyright-holder := 'Lele Gaifax'
src-dir := 'src' / replace(package-name, ".", "/")
locale-dir := src-dir / 'locale'
locale-domain := replace(package-name, ".", "-")
locale-potfile := locale-dir / locale-domain + ".pot"

# Extract translatable messages and translation catalogs
update-catalogs:
  pybabel --quiet extract --project={{package-name}} --version={{package-version}} \
          --add-comments=TRANSLATORS: --msgid-bugs-address={{bugs-email-address}} \
          --copyright-holder={{quote(copyright-holder)}} \
          --mapping={{locale-dir}}/mapping.cfg --output={{locale-potfile}} \
          --strip-comment-tags --sort-output {{src-dir}}
  pybabel --quiet update --output-dir={{locale-dir}} --previous --domain {{locale-domain}} \
          --input-file {{locale-dir}}/{{locale-domain}}.pot

# Compile translation catalogs
compile-catalogs:
  pybabel --quiet compile --directory={{locale-dir}} --domain={{locale-domain}} --statistics

# Initialize new translation language
add-new-locale locale:
  pybabel --quiet init --output-dir={{locale-dir}} --locale={{locale}} \
          --domain={{locale-domain}} --input-file {{locale-dir}}/{{locale-domain}}.pot


#################
# RELEASE TASKS #
#################

release-hint := '''
  >>>
  >>> Do your duties (update CHANGES.rst for example), then
  >>> execute “just tag-release”.
  >>>
'''

# Release new major/minor/dev version
release *kind='minor': && update-catalogs
    bump-my-version bump {{kind}}
    @echo '{{release-hint}}'

# Assert presence of release timestamp in CHANGES.rst
_check-release-date:
    @fgrep -q "{{package-version}} ({{`date --iso-8601`}})" CHANGES.rst \
      || (echo "ERROR: release date of version {{package-version}} not set in CHANGES.rst"; exit 1)

# Assert that everything has been committed
_assert-clean-tree:
    @test -z "`git status -s --untracked=no`" || (echo "UNCOMMITTED STUFF" && false)

# Check dist metadata
_check-dist:
    pyproject-build --sdist
    twine check dist/{{dist-file-name}}.tar.gz

# Tag new version
tag-release: _check-release-date _check-dist
    git commit -a -m "Release {{package-version}}"
    git tag -a -m "Version {{package-version}}" v{{package-version}}

# Build sdist and wheel
build: _assert-clean-tree
    pyproject-build

# Build and upload to [Test]PyPI
upload *repo='pypi': build
    twine upload --repository={{repo}} dist/{{dist-file-name}}.tar.gz dist/{{dist-file-name}}*.whl

# Upload to PyPI and push commits and tag to Gitlab
publish: upload
    git push
    git push --tags
